FROM node:boron

# crear directorio de la app
RUN mkdir -p /usr/src/app
WORKDIR /usr/src/app

# instalar dependencias para esto copio el package.json a la carpeta
COPY package.json /usr/src/app
RUN npm install

# Empaquetar código
COPY . /usr/src/app

EXPOSE 8081
CMD [ "npm", "start" ]
